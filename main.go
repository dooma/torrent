package main

import (
	"crypto/md5"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"regexp"
	"time"

	"bitbucket.org/me/torrent/torrent"
)

var files = make(map[string][]byte)

const (
	// ChunkSize the total number of bytes sent over socket
	ChunkSize  int = 1024
	// MaxRetries number of retries to complete a goal
	MaxRetries int = 5
)

// Send Wrapper function to send message over TCPConn
func Send(conn *net.TCPConn, message *torrent.Message) error {
	length := make([]byte, 4)
	binary.BigEndian.PutUint32(length, uint32(message.XXX_Size()))

	_, err := conn.Write(length)
	if err != nil {
		return err
	}

	chunk := make([]byte, 0)
	chunk, err = message.XXX_Marshal(chunk, false)
	if err != nil {
		return err
	}

	_, err = conn.Write(chunk)
	if err != nil {
		return err
	}

	return nil
}

// Receive Wrapper function to get message from socket
func Receive(conn *net.TCPConn, message *torrent.Message) (*torrent.Message, error) {
	reply := make([]byte, 4)
	_, err := conn.Read(reply)
	if err != nil {
		return nil, err
	}
	length := binary.BigEndian.Uint32(reply)

	buffer := make([]byte, length)
	_, err = conn.Read(buffer)
	if err != nil {
		return nil, err
	}
	message.Reset()
	message.XXX_Unmarshal(buffer)

	return message, nil
}

// Subscribe sends a subscribe request to Hub
func Subscribe(hub *net.TCPAddr, node *net.TCPAddr) {
	conn, err := net.DialTCP("tcp", nil, hub)
	if err != nil {
		log.Fatalln("Error on open socket for subscribe")
	}
	defer conn.Close()

	registration := &torrent.RegistrationRequest{
		Owner: "calins",
		Index: int32(node.Port%5004) + 1,
		Port:  int32(node.Port),
	}

	message := &torrent.Message{
		Type:                torrent.Message_REGISTRATION_REQUEST,
		RegistrationRequest: registration,
	}

	Send(conn, message)

	message.Reset()
	Receive(conn, message)
}

// CreateConnection Wrapper to initiate a TCP connection and drop if something is not reliable
func CreateConnection(node *net.TCPAddr) (*net.TCPConn, error) {
	conn, err := net.DialTCP("tcp", nil, node)
	if err != nil {
		log.Fatalln("Error on open socket for connection")
	}
	timeout := time.Now().Add(5 * time.Minute)

	conn.SetDeadline(timeout)
	return conn, nil
}

// ParseNodes gets a list of NodeId and returns a list of TCPAddr 
func ParseNodes(nodes []*torrent.NodeId) ([]*net.TCPAddr, error) {
	result := []*net.TCPAddr{}
	for _, node := range nodes {
		ip, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%v:%v", node.GetHost(), node.GetPort()))
		if err != nil {
			return nil, err
		}
		result = append(result, ip)
	}

	return result, nil
}

// ComputeFileInfo gets a filename and returns FileInfo
func ComputeFileInfo(filename string, data []byte) torrent.FileInfo {
	chunks := []*torrent.ChunkInfo{}
	index := 0
	for i := 0; i < len(data)/ChunkSize; i++ {
		index = i
		chunk := data[i*ChunkSize : (i+1)*ChunkSize]
		hash := md5.Sum(chunk)
		chunkInfo := &torrent.ChunkInfo{
			Index: uint32(index),
			Size:  uint32(len(chunk)),
			Hash:  hash[:],
		}
		chunks = append(chunks, chunkInfo)
	}

	if len(data)%ChunkSize != 0 {
		position := len(data) - len(data)%ChunkSize
		chunk := data[position:]
		hash := md5.Sum(chunk)
		chunkInfo := &torrent.ChunkInfo{
			Index: uint32(index + 1),
			Size:  uint32(len(chunk)),
			Hash:  hash[:],
		}
		chunks = append(chunks, chunkInfo)
	}

	hash := md5.Sum(data)
	fileInfo := torrent.FileInfo{
		Hash:     hash[:],
		Size:     uint32(len(data)),
		Filename: filename,
		Chunks:   chunks,
	}
	return fileInfo
}

// ResolveSubnet get list of nodes based on subnet
func ResolveSubnet(hub *net.TCPAddr, subnetID int32) ([]*torrent.NodeId, error) {
	subnetRequest := &torrent.SubnetRequest{
		SubnetId: subnetID,
	}
	subnetRequestMessage := &torrent.Message{
		Type:          torrent.Message_SUBNET_REQUEST,
		SubnetRequest: subnetRequest,
	}
	// Should connect to HUB instead of replying on socket.
	hubConn, err := CreateConnection(hub)
	defer hubConn.Close()
	if err != nil {
		return nil, err
	}

	Send(hubConn, subnetRequestMessage)
	message := &torrent.Message{}
	Receive(hubConn, message)
	response := message.GetSubnetResponse()
	if response.GetStatus() != torrent.Status_SUCCESS {
		// Reply error as response
	}
	return response.GetNodes(), nil
}

// HandleConnection goroutine that handles every request to this node
func HandleConnection(conn *net.TCPConn, hub *net.TCPAddr) {
	timeout := time.Now().Add(5 * time.Minute)

	conn.SetDeadline(timeout)
	defer conn.Close()

	message := &torrent.Message{}
	message, err := Receive(conn, message)
	if err != nil {
		log.Println(err.Error())
		return
	}

	switch message.GetType() {
	case torrent.Message_LOCAL_SEARCH_REQUEST:
		request := message.GetLocalSearchRequest()
		fileInfos := []*torrent.FileInfo{}

		if request == nil {
			message := &torrent.Message{
				Type: torrent.Message_LOCAL_SEARCH_RESPONSE,
				LocalSearchResponse: &torrent.LocalSearchResponse{
					Status:       torrent.Status_PROCESSING_ERROR,
					ErrorMessage: "Invalid message",
					FileInfo:     fileInfos,
				},
			}
			Send(conn, message)
			return
		}

		validFileName, err := regexp.Compile(request.GetRegex())
		if err != nil {
			message := &torrent.Message{
				Type: torrent.Message_LOCAL_SEARCH_RESPONSE,
				LocalSearchResponse: &torrent.LocalSearchResponse{
					Status:       torrent.Status_MESSAGE_ERROR,
					ErrorMessage: "Invalid regexp",
					FileInfo:     fileInfos,
				},
			}
			Send(conn, message)
			return
		}

		for fileName, data := range files {
			if validFileName.MatchString(fileName) {
				fileInfo := ComputeFileInfo(fileName, data)
				fileInfos = append(fileInfos, &fileInfo)
			}
		}

		message := &torrent.Message{
			Type: torrent.Message_LOCAL_SEARCH_RESPONSE,
			LocalSearchResponse: &torrent.LocalSearchResponse{
				Status:       torrent.Status_SUCCESS,
				ErrorMessage: "",
				FileInfo:     fileInfos,
			},
		}
		Send(conn, message)
	case torrent.Message_SEARCH_REQUEST:
		request := message.GetSearchRequest()
		nodeSearchResults := []*torrent.NodeSearchResult{}

		// Compute results
		nodes, err := ResolveSubnet(hub, request.GetSubnetId())
		if err != nil {
			// Reply with error
		}
		tcpAddrs, err := ParseNodes(nodes)
		if err != nil {
			// Reply with error
		}

		localSearchRequest := &torrent.Message{
			Type: torrent.Message_LOCAL_SEARCH_REQUEST,
			LocalSearchRequest: &torrent.LocalSearchRequest{
				Regex: request.GetRegex(),
			},
		}

		localSearchMessage := &torrent.Message{}

		for nodeID, node := range tcpAddrs {
			localSearchMessage.Reset()
			nodeConn, err := CreateConnection(node)
			if err != nil {
				continue
			}
			Send(nodeConn, localSearchRequest)
			Receive(nodeConn, localSearchMessage)

			localSearchResponse := localSearchMessage.GetLocalSearchResponse()

			nodeSearchResult := &torrent.NodeSearchResult{
				Node:         nodes[nodeID],
				Status:       localSearchResponse.GetStatus(),
				ErrorMessage: localSearchResponse.GetErrorMessage(),
				Files:        localSearchResponse.GetFileInfo(),
			}
			nodeSearchResults = append(nodeSearchResults, nodeSearchResult)
		}

		message := &torrent.Message{
			Type: torrent.Message_SEARCH_RESPONSE,
			SearchResponse: &torrent.SearchResponse{
				Status:       torrent.Status_SUCCESS,
				ErrorMessage: "",
				Results:      nodeSearchResults,
			},
		}
		Send(conn, message)
	case torrent.Message_UPLOAD_REQUEST:
		request := message.GetUploadRequest()
		filename := request.GetFilename()

		if len(filename) == 0 {
			message := &torrent.Message{
				Type: torrent.Message_UPLOAD_RESPONSE,
				UploadResponse: &torrent.UploadResponse{
					Status:       torrent.Status_MESSAGE_ERROR,
					ErrorMessage: "Invalid filename",
					FileInfo:     nil,
				},
			}
			Send(conn, message)
			return
		}

		data := request.GetData()
		files[filename] = data

		chunks := []*torrent.ChunkInfo{}
		index := 0
		for i := 0; i < len(data)/ChunkSize; i++ {
			index = i
			chunk := data[i*ChunkSize : (i+1)*ChunkSize]
			hash := md5.Sum(chunk)
			chunkInfo := &torrent.ChunkInfo{
				Index: uint32(index),
				Size:  uint32(len(chunk)),
				Hash:  hash[:],
			}
			chunks = append(chunks, chunkInfo)
		}

		if len(data)%ChunkSize != 0 {
			position := len(data) - len(data)%ChunkSize
			chunk := data[position:]
			hash := md5.Sum(chunk)
			chunkInfo := &torrent.ChunkInfo{
				Index: uint32(index + 1),
				Size:  uint32(len(chunk)),
				Hash:  hash[:],
			}
			chunks = append(chunks, chunkInfo)
		}

		hash := md5.Sum(data)
		fileInfo := &torrent.FileInfo{
			Hash:     hash[:],
			Size:     uint32(len(data)),
			Filename: filename,
			Chunks:   chunks,
		}

		response := &torrent.UploadResponse{
			Status:       torrent.Status_SUCCESS,
			ErrorMessage: "",
			FileInfo:     fileInfo,
		}

		message.Reset()

		message = &torrent.Message{
			Type:           torrent.Message_UPLOAD_RESPONSE,
			UploadResponse: response,
		}
		Send(conn, message)
	case torrent.Message_REPLICATE_REQUEST:
		request := message.GetReplicateRequest()
		fileInfo := request.GetFileInfo()

		if len(fileInfo.GetFilename()) == 0 {
			message := &torrent.Message{
				Type: torrent.Message_REPLICATE_RESPONSE,
				ReplicateResponse: &torrent.ReplicateResponse{
					Status:       torrent.Status_MESSAGE_ERROR,
					ErrorMessage: "Invalid filename",
				},
			}
			Send(conn, message)
			return
		}

		if _, ok := files[fileInfo.GetFilename()]; ok {
			replicateResponse := &torrent.ReplicateResponse{
				Status:       torrent.Status_SUCCESS,
				ErrorMessage: "",
			}
			response := &torrent.Message{
				Type:              torrent.Message_REPLICATE_RESPONSE,
				ReplicateResponse: replicateResponse,
			}
			Send(conn, response)
			return
		}
		nodes, err := ResolveSubnet(hub, request.GetSubnetId())
		if err != nil {
			message := &torrent.Message{
				Type: torrent.Message_REPLICATE_RESPONSE,
				ReplicateResponse: &torrent.ReplicateResponse{
					Status:       torrent.Status_PROCESSING_ERROR,
					ErrorMessage: "Cannot process subnet",
				},
			}
			Send(conn, message)
			return
		}
		tcpAddrs, err := ParseNodes(nodes)
		if err != nil {
			message := &torrent.Message{
				Type: torrent.Message_REPLICATE_RESPONSE,
				ReplicateResponse: &torrent.ReplicateResponse{
					Status:       torrent.Status_PROCESSING_ERROR,
					ErrorMessage: "Invalid subnet response",
				},
			}
			Send(conn, message)
			return
		}
		chunkRequest := &torrent.ChunkRequest{
			FileHash:   fileInfo.GetHash(),
			ChunkIndex: 0,
		}
		chunkRequestMessage := &torrent.Message{
			Type:         torrent.Message_CHUNK_REQUEST,
			ChunkRequest: chunkRequest,
		}
		chunkRequestResponse := &torrent.Message{}

		chunksLeft := fileInfo.GetChunks()
		nodeReplicationStatus := &torrent.NodeReplicationStatus{}
		nodeReplicationStatuses := []*torrent.NodeReplicationStatus{}
		receivedChunks := make(map[int][]byte)

		for nodeID, node := range tcpAddrs {
			for _, chunk := range chunksLeft {
				nodeConn, err := CreateConnection(node)
				if err != nil {
					nodeReplicationStatus := &torrent.NodeReplicationStatus{
						Node:         nodes[nodeID],
						Status:       torrent.Status_NETWORK_ERROR,
						ErrorMessage: "Cannot connect to node",
					}
	
					nodeReplicationStatuses = append(nodeReplicationStatuses, nodeReplicationStatus)
					break
				}

				chunkRequest.ChunkIndex = chunk.GetIndex()
				Send(nodeConn, chunkRequestMessage)
				Receive(nodeConn, chunkRequestResponse)
				nodeConn.Close()
				chunkResponse := chunkRequestResponse.GetChunkResponse()

				if chunkResponse != nil {
					switch chunkResponse.GetStatus() {
					case torrent.Status_SUCCESS:
						receivedChunks[int(chunk.GetIndex())] = chunkResponse.GetData()
					case torrent.Status_MESSAGE_ERROR:
					case torrent.Status_UNABLE_TO_COMPLETE:
					case torrent.Status_PROCESSING_ERROR:
						log.Println(chunkResponse)
						log.Fatalln("Fix implementation")
					}
					nodeReplicationStatus = &torrent.NodeReplicationStatus{
						Node:         nodes[nodeID],
						ChunkIndex:   chunk.GetIndex(),
						Status:       chunkResponse.GetStatus(),
						ErrorMessage: chunkResponse.GetErrorMessage(),
					}
					nodeReplicationStatuses = append(nodeReplicationStatuses, nodeReplicationStatus)
				}
			}
		}

		for _, chunk := range chunksLeft {
			index := int(chunk.GetIndex())
			if value, ok := receivedChunks[index]; ok {
				files[fileInfo.GetFilename()] = append(files[fileInfo.GetFilename()], value...)
			} else {
				files[fileInfo.GetFilename()] = append(files[fileInfo.GetFilename()], make([]byte, ChunkSize)...)
			}
		}

		message = &torrent.Message{
			Type: torrent.Message_REPLICATE_RESPONSE,
			ReplicateResponse: &torrent.ReplicateResponse{
				Status:         torrent.Status_SUCCESS,
				ErrorMessage:   "",
				NodeStatusList: nodeReplicationStatuses,
			},
		}
		Send(conn, message)
	case torrent.Message_CHUNK_REQUEST:
		request := message.GetChunkRequest()
		fileHash := request.GetFileHash()
		index := int(request.GetChunkIndex())

		if len(fileHash) != 16 || index < 0 {
			message := &torrent.Message{
				Type: torrent.Message_CHUNK_RESPONSE,
				ChunkResponse: &torrent.ChunkResponse{
					Status:       torrent.Status_MESSAGE_ERROR,
					ErrorMessage: "",
					Data:         []byte{},
				},
			}
			Send(conn, message)
			return
		}

		var data []byte = nil

		for _, value := range files {
			hash := md5.Sum(value)
			if string(hash[:]) == string(fileHash) {
				data = value
				break
			}
		}

		if data == nil {
			message := &torrent.Message{
				Type: torrent.Message_CHUNK_RESPONSE,
				ChunkResponse: &torrent.ChunkResponse{
					Status:       torrent.Status_UNABLE_TO_COMPLETE,
					ErrorMessage: "",
					Data:         []byte{},
				},
			}
			Send(conn, message)
			return
		}

		if len(data) < index*ChunkSize {
			message := &torrent.Message{
				Type: torrent.Message_CHUNK_RESPONSE,
				ChunkResponse: &torrent.ChunkResponse{
					Status:       torrent.Status_PROCESSING_ERROR,
					ErrorMessage: "",
					Data:         []byte{},
				},
			}
			Send(conn, message)
			return
		}

		chunk := []byte{}

		if index < len(data)/ChunkSize {
			chunk = data[index*ChunkSize : (index+1)*ChunkSize]
		} else {
			chunk = data[index*ChunkSize:]
		}

		message := &torrent.Message{
			Type: torrent.Message_CHUNK_RESPONSE,
			ChunkResponse: &torrent.ChunkResponse{
				Status:       torrent.Status_SUCCESS,
				ErrorMessage: "",
				Data:         chunk,
			},
		}
		Send(conn, message)
	case torrent.Message_DOWNLOAD_REQUEST:
		request := message.GetDownloadRequest()
		fileHash := request.GetFileHash()

		if len(fileHash) != 16 {
			message := &torrent.Message{
				Type: torrent.Message_DOWNLOAD_RESPONSE,
				DownloadResponse: &torrent.DownloadResponse{
					Status:       torrent.Status_MESSAGE_ERROR,
					ErrorMessage: "",
					Data:         []byte{},
				},
			}
			Send(conn, message)
			return
		}

		var data []byte = nil

		for _, value := range files {
			hash := md5.Sum(value)
			if string(hash[:]) == string(fileHash) {
				data = value
				break
			}
		}

		if data == nil {
			message := &torrent.Message{
				Type: torrent.Message_DOWNLOAD_RESPONSE,
				DownloadResponse: &torrent.DownloadResponse{
					Status:       torrent.Status_UNABLE_TO_COMPLETE,
					ErrorMessage: "",
					Data:         []byte{},
				},
			}
			Send(conn, message)
		} else {
			message := &torrent.Message{
				Type: torrent.Message_DOWNLOAD_RESPONSE,
				DownloadResponse: &torrent.DownloadResponse{
					Status:       torrent.Status_SUCCESS,
					ErrorMessage: "",
					Data:         data,
				},
			}
			Send(conn, message)
		}
	default:
		log.Println("Nothing")
	}
}

func main() {
	log.Println("Torr node")

	file, err := os.OpenFile("torrent.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	multiWriter := io.MultiWriter(os.Stdout, file)
	log.SetOutput(multiWriter)

	if len(os.Args) < 5 || len(os.Args)%2 == 0 {
		log.Fatalln("Insufficient number of arguments")
	}

	hub, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%v:%v", os.Args[1], os.Args[2]))
	if err != nil {
		log.Fatalln(err)
	}

	node, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%v:%v", os.Args[3], os.Args[4]))
	if err != nil {
		log.Fatalln(err)
	}

	go Subscribe(hub, node)

	listener, err := net.ListenTCP("tcp", node)
	if err != nil {
		log.Fatalln(err)
	}

	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Fatal(err)
		}
		go HandleConnection(conn, hub)
	}
}
