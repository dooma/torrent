build:
	go build -o ./bin ./...

run%: build
	./bin/torrent 127.0.0.1 5000 127.0.0.1 $*

master:
	./torr-darwin-amd64 127.0.0.1 5000 127.0.0.1 5001 5002 5003

run-all: | run5004 run5005 run5006
